## Todo

Este repositorio utiliza las siguientes tecnologías: [Laravel](https://laravel.com/), [React](https://reactjs.org/),
[Bootstrap](https://getbootstrap.com/).

Está pensado para ser utilizado con [PHP Desktop](https://github.com/cztomczak/phpdesktop)

#### Requisitos:

Debes tener instalado [Node.js](https://nodejs.org/en/download/) y [PHP](https://www.php.net/).

Sigue los siguientes pasos: 

1. Abre la terminal y ejecuta: `git clone https://Tanito_Fantini@bitbucket.org/Tanito_Fantini/todo-desktop.git` para descargar el projecto.
2. Ejecuta el comando: `cd todo-desktop` para entrar al directorio del proyecto.
3. Ejecuta el comando: `composer install` para instalar dependencias.
4. Ejecuta el comando: `npm install` para instalar dependencias.
5. Ejecuta el comando: `npm run prod` para generar los archivos.
6. Ejecuta el comando: `touch database/database.sqlite` para generar la base de datos (SQLite).
7. Ejecuta el comando: `cp .env.example .env` para generar el archivo de configuración.
7. Ejecuta el comando: `php artisan key:generate` para generar la llave.
8. En el archivo de configuración .env establece `DB_CONNECTION=sqlite` y borra los demás parámetros de la base de datos.
9. Ejecuta el comando: `php artisan migrate` para crear las tablas y datos de la base de datos.
10. Ejecuta el comando: `php artisan serve` para correr la aplicación.
11. Abre en tu navegador el siguiente URL [http://127.0.0.1:8000](http://127.0.0.1:8000).