<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use App\Models\Task;

class Project extends Model {

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'projects';

  /**
   * The primary key associated with the table.
   *
   * @var string
   */
  protected $primaryKey = 'id';
  
  
  protected $fillable = ['name', 'description'];

  /**
   * [boot description]
   * 
   * @return [type] [description]
   */
  public static function boot() {
    parent::boot();

    static::deleting(function($model) {
      Log::info('deleting! - ' . $model->name . ' - ' . $model->id);

      $deletedRows = Task::where('project_id', $model->id)->delete();
    });
  }

  // ---------------------------------------------------------------------------------------------------------------------

  public function tasks() {
    return $this->hasMany(Task::class);
  }

  // ---------------------------------------------------------------------------------------------------------------------

}
