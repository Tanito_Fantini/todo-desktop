<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use App\Models\Task;

class TaskController extends Controller {

  // ---------------------------------------------------------------------------------------------------------------------
  
  /**
   * Get the task requested
   *
   * @return void
   */
  public function show($id) {
    $r = ['error' => 0, 'task' => []];

    try {
      $task = Task::find($id);

      $r['task'] = $task;
    }
    catch(QueryException $ex) {
      $r['error']   = 1;
      $r['message'] = $ex->getMessage();
    }

    return $r;
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * Create/update a task
   *
   * @return void
   */
  public function store(Request $request, $id = 0) {
    $r = ['error' => 0];

    $validatedData = $request->validate([
      'id'         => 'nullable|integer',
      'id_project' => 'required|integer',
      'date'       => 'required|date',
      'priority'   => 'required|in:low,normal,hight',
      'content'    => 'required|max:255'
    ]);

    try {
      if( $id == 0 ) {
        $task = Task::create([
          'project_id' => $validatedData['id_project'],
          'date'       => $validatedData['date'],
          'priority'   => $validatedData['priority'],
          'content'    => $validatedData['content']
        ]);

        $r['id'] = $task->id;
      }
      else {
        $task = Task::find($id);

        $task->date = $validatedData['date'];
        $task->priority = $validatedData['priority'];
        $task->content = $validatedData['content'];
        $task->save();
      }
    }
    catch(QueryException $ex) {
      $r['error']   = 1;
      $r['message'] = $ex->getMessage();
    }

    return $r;
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * Remove the task requested
   *
   * @return void
   */
  public function remove(Request $request, $id = 0) {
    $r = ['error' => 0];

    try {
      $task = Task::findOrFail($id);
      $task->delete();
    }
    catch(QueryException $ex) {
      $r['error']   = 1;
      $r['message'] = $ex->getMessage();
    }

    return $r;
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * 
   * @return void
   */
  public function datatable(Request $request, $project_id = 0) {
    $r = [
      'error'           => '',
      'draw'            => intval($request->input('draw')),
      'recordsTotal'    => 0,
      'recordsFiltered' => 0,
      'data'            => []
    ];

    // Array of database columns which should be read and sent back to DataTables.
    // The `db` parameter represents the column name in the database, while the `dt`
    // parameter represents the DataTables column identifier.
    $columns = [
      [
        'db' => 'date',
        'dt' => 0
      ],
      [
        'db' => 'priority',
        'dt' => 1
      ],
      [
        'db' => 'content',
        'dt' => 2
      ],
      [
        'db' => 'btns',
        'dt' => 3
      ]
    ];

    // Table's primary key
    $primaryKey = 'id';

    $vars_sql = varsDatatable($columns, $request->query());
    // dd($vars_sql);

    $where = $vars_sql['where'];
    $order = $vars_sql['order'];
    $limit = $vars_sql['limit'];

    if( is_numeric($project_id) ) {
      if( $where === '' ) {
        $where = ' WHERE project_id = ' . $project_id;
      }
      else {
        $where .= ' AND project_id = ' . $project_id;
      }
    }
    else {
      switch( $project_id ) {
        case 'hoy':
          $date = date('Y-m-d');

          if( $where === '' ) {
            $where = " WHERE date = '{$date}'";
          }
          else {
            $where .= " AND date = '{$date}'";
          }
          break;
        
        case '7dias':
          $from = date('Y-m-d');
          $date = strtotime($from);
          $date = strtotime('+7 day', $date);
          $to   = date('Y-m-d', $date);

          if( $where === '' ) {
            $where = " WHERE date >= '{$from}' AND date <= '{$to}'";
          }
          else {
            $where .= " AND date >= '{$from}' AND date <= '{$to}'";
          }
          break;
        
        case 'mes':
          $from = date('Y-m-01');
          $to   = date('Y-m-t');

          if( $where === '' ) {
            $where = " WHERE date >= '{$from}' AND date <= '{$to}'";
          }
          else {
            $where .= " AND date >= '{$from}' AND date <= '{$to}'";
          }
          break;
        
        default:
          if( $where === '' ) {
            $where = ' WHERE project_id = 0';
          }
          else {
            $where .= ' AND project_id = 0';
          }
      }
    }

    try {
      $sql = "SELECT id, date, priority, content
              FROM tasks
              $where
              $order
              $limit;";
      // dd( $sql );
      $tasks = DB::select($sql);

      $r['recordsFiltered'] = count($tasks);

      foreach( $tasks as $task ) {
        $date = explode('-', $task->date);
        $date = $date[2] . '/' . $date[1] . '/' . $date[0];

        $priority = '';

        switch( $task->priority ) {
          case 'low':
            $priority = 'Baja';
            break;
          
          case 'normal':
            $priority = 'Normal';
            break;

          case 'hight':
            $priority = 'Alta';
            break;
        }

        $content = '<div class="task-content">' . $task->content . '</div>';

        $btns = '<div class="info" data-id="' . $task->id . '"></div>
                 <div class="text-center">
                   <div class="btn-group btn-group-sm" role="group" aria-label="">
                     <button type="button" class="btn btn-secondary btn-edit" title="editar la Tarea">
                       <i class="fas fa-pen"></i>
                     </button>
                     <button type="button" class="btn btn-danger btn-remove" title="eliminar la Tarea">
                       <i class="fas fa-trash"></i>
                     </button>
                   </div>
                 </div>';

        $r['data'][] = [
          '<div class="text-center">' . $date . '</div>',
          $priority,
          $content,
          $btns
        ];
      }

      $sql = "SELECT COUNT($primaryKey) AS total
              FROM tasks
              WHERE project_id = ?;";
      $tasks = DB::select($sql, [$project_id]);
      $r['recordsTotal'] = $tasks[0]->total;
    }
    catch(QueryException $ex) {
      $r['error']   = 1;
      $r['message'] = $ex->getMessage();
    }

    return $r;
  }

  // ---------------------------------------------------------------------------------------------------------------------

}
