<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Models\Project;

class ProjectController extends Controller {

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * Get all projects
   *
   * @return void
   */
  public function index() {
    $r = ['error' => 0, 'projects' => []];
    
    try {
      $projects = Project::withCount('tasks')->orderBy('created_at', 'desc')->get();
      // dd( $projects );

      foreach( $projects as $p ) {
        $p->tasks_count = (int)$p->tasks_count;
      }

      $r['projects'] = $projects;
    }
    catch(QueryException $ex) {
      $r['error']   = 1;
      $r['message'] = $ex->getMessage();
    }

    return $r;
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * Get the project requested
   *
   * @return void
   */
  public function show($id) {
    $r = ['error' => 0, 'project' => []];

    try {
      $project = Project::find($id);

      $r['project'] = $project;
    }
    catch(QueryException $ex) {
      $r['error']   = 1;
      $r['message'] = $ex->getMessage();
    }

    return $r;
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * Create/update a project
   *
   * @return void
   */
  public function store(Request $request, $id = 0) {
    $r = ['error' => 0];

    $validatedData = $request->validate([
      'id'          => 'nullable|integer',
      'name'        => 'required|min:3|max:50',
      'description' => 'max:255',
    ]);

    try {
      if( $id == 0 ) {
        $project = Project::create([
          'name'        => $validatedData['name'],
          'description' => $validatedData['description']
        ]);

        $r['id'] = $project->id;
      }
      else {
        $project = Project::find($id);

        $project->name = $validatedData['name'];
        $project->description = $validatedData['description'];
        $project->save();
      }
    }
    catch(QueryException $ex) {
      $r['error']   = 1;
      $r['message'] = $ex->getMessage();
    }

    return $r;
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * Remove the project requested
   *
   * @return void
   */
  public function remove(Request $request, $id = 0) {
    $r = ['error' => 0];

    try {
      $project = Project::findOrFail($id);
      $project->delete();
    }
    catch(QueryException $ex) {
      $r['error']   = 1;
      $r['message'] = $ex->getMessage();
    }

    return $r;
  }

  // ---------------------------------------------------------------------------------------------------------------------

}
