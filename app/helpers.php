<?php

if( !function_exists('pluck') ) {
  /**
   * Pull a particular property from each assoc. array in a numeric array, 
   * returning and array of the property values from each item.
   *
   *  @param  array  $a    Array to get data from
   *  @param  string $prop Property to read
   *  @return array        Array of property values
   */
  function pluck($a, $prop) {
    $out = [];

    for( $i=0, $len=count($a) ; $i<$len ; $i++ ) {
      $out[] = $a[$i][$prop];
    }

    return $out;
  }
}

if( !function_exists('varsDatatable') ) {
  /**
   * 
   * 
   * @param  array $columns
   * @param  array $vars_sql
   * @param  array $columns_extra
   * @return array
   */
  function varsDatatable($columns = [], $vars_sql = [], $columns_extra = []) {
    $vars = [
      'limit' => '',
      'order' => '',
      'where' => ''
    ];

    if( is_array($columns) && count($columns) ) {
      /*
       * Paging
       */
      if( isset($vars_sql['start']) && $vars_sql['length'] != -1 ) {
        $vars['limit'] = 'LIMIT ' . (int)$vars_sql['start'] . ', ' . (int)$vars_sql['length'];
      }

      /*
       * Ordering
       */
      $order = '';
      if( isset($vars_sql['order']) && count($vars_sql['order']) ) {
        $orderBy   = [];
        $dtColumns = pluck($columns, 'dt');

        // recorremos las columas por las que hay que ordenar
        for( $i=0, $ien=count($vars_sql['order']); $i<$ien; $i++ ) {
          // Convert the column index into the column data property
          $columnIdx     = (int)$vars_sql['order'][$i]['column'];
          $requestColumn = $vars_sql['columns'][$columnIdx];

          $columnIdx = array_search( $requestColumn['data'], $dtColumns );
          $column    = $columns[ $columnIdx ];

          if( $requestColumn['orderable'] == 'true' ) {
            $dir = $vars_sql['order'][$i]['dir'] === 'asc' ?
              'ASC' :
              'DESC';

            $orderBy[] = $column['db'].' '.$dir;
          }
        }

        $vars['order'] = 'ORDER BY ' . implode(', ', $orderBy);
      }

      /* 
       * Filtering
       */
      $global_search = $vars_sql['search'];
      $bindings      = [];
      $globalSearch  = [];
      $columnSearch  = [];
      $dtColumns     = pluck($columns, 'dt');

      if( isset($global_search['value']) ) {
        $global_search['value'] = trim($global_search['value']);

        if( $global_search['value'] != '' ) {
          // recorremos las columnas enviadas
          for( $i=0, $ien=count($vars_sql['columns']); $i<$ien; $i++ ) {
            $requestColumn = $vars_sql['columns'][$i];
            $columnIdx     = array_search( $requestColumn['data'], $dtColumns );
            $column        = $columns[ $columnIdx ];

            if( $requestColumn['searchable'] == 'true' ) {
              $globalSearch[] = $column['db'] . " LIKE '%" . ($global_search['value']) . "%'";
            }
          }

          // si se pasaron columnas extra
          if( is_array($columns_extra) ) {
            foreach( $columns_extra as $c_extra ) {
              $globalSearch[] = $c_extra . " LIKE '%" . ($global_search['value']) . "%'";
            }
          }
        }
      }

      // Individual column filtering
      for( $i=0, $ien=count($vars_sql['columns']); $i<$ien; $i++ ) {
        $requestColumn = $vars_sql['columns'][$i];
        $columnIdx     = array_search( $requestColumn['data'], $dtColumns );
        $column        = $columns[ $columnIdx ];

        $str = $requestColumn['search']['value'];

        if( $requestColumn['searchable'] == 'true' && $str != '' ) {
          $columnSearch[] = $column['db'] . " LIKE '%" . $str . "%'";
        }
      }

      // Combine the filters into a single string
      $where = '';

      if( count($globalSearch) ) {
        $where = '(' . implode(' OR ', $globalSearch).')';
      }

      if( count($columnSearch) ) {
        $where = $where === '' ?
          implode(' AND ', $columnSearch) :
          $where .' AND '. implode(' AND ', $columnSearch);
      }

      if( $where !== '' ) {
        $vars['where'] = 'WHERE ' . $where;
      }
    }

    return $vars;
  }
}