import axios from 'axios'

class Project {
  constructor() {
  }

  get(id) {
    return axios({
      method: 'get',
      url: '/api/projects/' + id
    });
  }

  getAll() {
    return axios({
      method: 'get',
      url: '/api/projects'
    });
  }

  create(project) {
    return axios({
      method: 'post',
      url: '/api/projects',
      data: project
    });
  }

  update(project) {
    return axios({
      method: 'put',
      url: '/api/projects/' + project.id,
      data: project
    });
  }

  remove(id) {
    return axios({
      method: 'delete',
      url: '/api/projects/' + id
    });
  }
}

export default Project;