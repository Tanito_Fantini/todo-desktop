import React, { Component, Fragment } from 'react';
// import Form from './FormProject';
import PropTypes from 'prop-types';

class Modal extends Component {
  constructor(props) {
    super(props);

    // DOM references
    this.element  = React.createRef();
  }

  // invoked immediately after a component is mounted
  componentDidMount() {
    $(this.element.current).modal({
      backdrop: this.props.backdrop !== undefined ? this.props.backdrop : true,
      keyboard: this.props.keyboard !== undefined ? this.props.keyboard : true,
      show: false
    });

    // it is fires immediately when the show instance method is called
    $(this.element.current).on('show.bs.modal', (e) => {
      if( typeof this.props.onOpen === "function" ) {
        this.props.onOpen(e);
      }
    });

    // it is fired when the modal has been made visible to the user
    $(this.element.current).on('shown.bs.modal', (e) => {
      if( typeof this.props.onVisible === "function" ) {
        this.props.onVisible(e);
      }
    });

    // it is fired immediately when the hide instance method has been called
    $(this.element.current).on('hide.bs.modal', (e) => {
      if( typeof this.props.onClose === "function" ) {
        this.props.onClose(e);
      }
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if( this.props.visible ) {
      $(this.element.current).modal('show');
    } else {
      $(this.element.current).modal('hide');
    }
  }

  render() {
    return (
      <Fragment>
        <div className="modal" tabIndex="-1" role="dialog" ref={this.element}>
          <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">
                  { this.props.title }
                </h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                { this.props.children }
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

Modal.propTypes = {
  visible: PropTypes.bool,
  title: PropTypes.string,
  onOpen: PropTypes.func,
  onVisible: PropTypes.func,
  onClose: PropTypes.func
};

export default Modal;