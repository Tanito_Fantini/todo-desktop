import React, { Component, Fragment } from 'react';
import Modal from './Modal';
import Form from './FormProject';
import PropTypes from 'prop-types';

class Projects extends Component {

  constructor(props) {
    super(props);

    // DOM references
    this.input_name = React.createRef();

    // binding project's functions
    this.showModal = this.showModal.bind(this);
  }

  // invoked immediately after a component is mounted
  componentDidMount() {
  }

  showModal(e) {
    this.input_name.current.select();
  }

  render() {
    var projects = [];

    if( this.props.data.length > 0 ) {
      projects = this.props.data.map((project) => {
        var
            name        = project.name,
            title       = project.name,
            description = project.description !== null ? project.description : '',
            tasks_count = project.tasks_count !== undefined ? project.tasks_count : 0,
            css_item    = 'list-project' + (project.id == this.props.id_selected ? ' selected' : '');

        if( name.length > 20 ) {
          name = name.substring(0, 15) + '...';
        } else {
          title = '';
        }

        return (
          <div className={ css_item } key={ project.id }>
            <div className="project">
              <span className="name" title={ title }>{ name }</span> <span className="count-tasks">({ tasks_count })</span>
            </div>
            <div className="project-description">{description}</div>

            <div className="btn-group btn-group-sm project-btns" role="group" aria-label="">
              <button type="button" className="btn btn-secondary" onClick={ () => { this.props.onEdit(project.id); } } title="editar Proyecto">
                <i className="fas fa-pen"></i>
              </button>
              <button type="button" className="btn btn-secondary" onClick={ () => { this.props.onSelect(project.id); } } title="tareas del Proyecto">
                <i className="fas fa-tasks"></i>
              </button>
              <button type="button" className="btn btn-danger" onClick={ () => { this.props.onRemove(project.id); } } title="eliminar Proyecto">
                <i className="fas fa-trash"></i>
              </button>
            </div>
          </div>
        );
      });
    }

    return (
      <Fragment>
        <h3>
          Proyectos
        </h3>

        {projects.length > 0 ? (
          <div className="list-projects">
            { projects }
          </div>
        ) : (
          <p>No hay Proyectos cargados</p>
        )}

        <Modal
          visible={ this.props.show_modal }
          title={ this.props.title_modal }
          onVisible={ this.showModal }
          onClose={ this.props.onCloseModal }
        >
          <Form
            input_focus={ this.input_name }
            project={ this.props.project }
            onSubmit={ this.props.onSubmit }
          />
        </Modal>
      </Fragment>
    );
  }
}

Projects.propTypes = {
  onSelect: PropTypes.func
};

export default Projects;