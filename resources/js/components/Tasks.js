import React, { Fragment } from 'react';
import Modal from './Modal';
import Form from './FormTask';
import Datatable from './Table';
import PropTypes from 'prop-types';

class Tasks extends React.Component {

  constructor(props) {
    super(props);

    // DOM references
    this.input_content = React.createRef();

    // binding task's functions
    this.showModal = this.showModal.bind(this);
  }

  showModal(e) {
    this.input_content.current.select();
  }

  render() {
    return (
      <Fragment>
        <Datatable
          options={ this.props.options_datatable }
          onCreate={ this.props.onCreateTable }
          onInit={ this.props.onInitTable }
        />

        <Modal
          visible={ this.props.show_modal }
          title={ this.props.title_modal }
          onVisible={ this.showModal }
          onClose={ this.props.onCloseModal }
        >
          <Form
            input_focus={ this.input_content }
            task={ this.props.task }
            onSubmit={ this.props.onSubmit }
          />
        </Modal>
      </Fragment>
    );
  }
}

export default Tasks;