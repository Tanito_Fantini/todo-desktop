import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import NavBar from './NavBar';
import Projects from './Projects';
import Tasks from './Tasks';
import Footer from './Footer';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import ProjectService from '../ProjectService';
import TaskService from '../TaskService';

const $ = require('jquery');

class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      // projects
      id_project_selected: 0,
      projects: [],
      project: {
        id: '',
        name: '',
        description: ''
      },
      modal_project: {
        show: false,
        title: ''
      },
      // tasks
      task: {
        id: '',
        date: new Date(),
        priority: 'normal',
        content: ''
      },
      modal_task: {
        show: false,
        title: ''
      },
      search_tasks: ''
    };

    this.api_table_task = null;

    // services
    this.projectService = new ProjectService();
    this.taskService    = new TaskService();

    // binding functions
    this.onClickMenu         = this.onClickMenu.bind(this);
    // binding project's functions
    this.onCreateProject     = this.onCreateProject.bind(this);
    this.onSelectProject     = this.onSelectProject.bind(this);
    this.onEditProject       = this.onEditProject.bind(this);
    this.onSubmitProject     = this.onSubmitProject.bind(this);
    this.onCloseModalProject = this.onCloseModalProject.bind(this);
    this.onRemoveProject     = this.onRemoveProject.bind(this);
    // binding task's functions
    this.onCreateTask        = this.onCreateTask.bind(this);
    this.onEditTask          = this.onEditTask.bind(this);
    this.onSubmitTask        = this.onSubmitTask.bind(this);
    this.onCloseModalTask    = this.onCloseModalTask.bind(this);
    this.onRemoveTask        = this.onRemoveTask.bind(this);
    this.onInitTableTasks    = this.onInitTableTasks.bind(this);
    this.onCreateTableTask   = this.onCreateTableTask.bind(this);
    this.reloadTableTasks    = this.reloadTableTasks.bind(this);
    this.onChangeSearchTask  = this.onChangeSearchTask.bind(this);
  }

  // invoked immediately after a component is mounted
  componentDidMount() {
    this.projectService
      .getAll()
      .then(response => {
        var state = {
          projects: []
        };

        if( !response.data.error ) {
          state.projects = response.data.projects;

          let data = localStorage.getItem("todo-react");

          // parse the localStorage string and setState
          try {
            if( data !== null ) {
              data = JSON.parse(data);

              state.id_project_selected = data.id_project_init;
            }
          }
          catch (e) {
          }

          this.setState(state, () => {
            if( this.state.id_project_selected > 0 ) {
              this.reloadTableTasks(this.state.id_project_selected);
            }
          });
        }
        else {
          toast.error(response.data.message);
        }
      })
      .catch(error => {
        toast.error(error);
      });

    document.addEventListener('keyup', event => {
      // shift + p
      if( event.shiftKey && event.keyCode === 80 ) {
        this.onCreateProject();
      }

      // shift + t
      if( event.shiftKey && event.which === 84 && this.state.id_project_selected > 0 ) {
        this.onCreateTask();
      }
    });
  }

  onClickMenu(type) {
    this.reloadTableTasks(type);

    this.setState({
      id_project_selected: type
    });
  }

  onCreateProject() {
    this.setState({
      modal_project: {
        show: true,
        title: "Cargar Proyecto"
      },
      project: {
        id: '',
        name: '',
        description: ''
      }
    });
  }

  onSelectProject(id) {
    localStorage.setItem('todo-react', JSON.stringify({
      id_project_init: id
    }));

    this.reloadTableTasks(id);

    this.setState({
      id_project_selected: id
    });
  }

  onEditProject(id) {
    this.projectService
      .get(id)
      .then(response => {
        if( !response.data.error ) {
          if( response.data.project === null ) {
            toast.error("No existe el Proyecto solicitado");
          }
          else {
            this.setState({
              modal_project: {
                show: true,
                title: "Editar Proyecto"
              },
              project: {
                id: response.data.project.id,
                name: response.data.project.name,
                description: response.data.project.description === null ? "" : response.data.project.description
              }
            });
          }
        }
        else {
          toast.error(response.data.message);
        }
      })
      .catch(error => {
        toast.error(error);
      });
  }

  onSubmitProject(project, actions) {
    let projects;

    // insert
    if( project.id === '' ) {
      this.projectService
        .create(project)
        .then(response => {
          if( !response.data.error ) {
            projects = [...this.state.projects];

            project.id = response.data.id;

            projects.push(project);

            this.setState({
              modal_project: {
                show: false,
                title: ''
              },
              project: {
                id: '',
                name: '',
                description: ''
              },
              projects
            });
          }
          else {
            toast.error(response.data.message);
          }
        })
        .catch(error => {
          toast.error(error);
        })
        .finally(() => {
          actions.resetForm();
        });
    }
    // update
    else {
      this.projectService
        .update(project)
        .then(response => {
          // console.log( response );

          projects = this.state.projects.map((p) => {
            if( p.id === project.id ) {
              p.name = project.name;
              p.description = project.description;
            }

            return p;
          });

          this.setState({
            modal_project: {
              show: false,
              title: ''
            },
            project: {
              id: '',
              name: '',
              description: ''
            },
            projects
          });
        })
        .catch(error => {
          toast.error(error);
        })
        .finally(() => {
          actions.resetForm();
        });
    }
  }

  onCloseModalProject() {
    const modal_project = { ...this.state.modal_project };

    modal_project.show = false;

    this.setState({
      modal_project
    });
  }

  onRemoveProject(id) {
    if( window.confirm("¿Eliminar el Proyecto?") ) {
      this.projectService
        .remove(id)
        .then(response => {
          if( !response.data.error ) {
            const projects = this.state.projects.filter((project) => project.id !== id);
            var id_project_selected = this.state.id_project_selected;

            // if removes the selected
            if( id == id_project_selected ) {
              this.reloadTableTasks();

              id_project_selected = 0;
            }

            this.setState({
              id_project_selected,
              projects
            });
          }
          else {
            toast.error(response.data.message);
          }
        })
        .catch(error => {
          toast.error(error);
        });
    }
  }

  onInitTableTasks(e, json) {
    // edit
    $(e.currentTarget).on("click", ".btn-edit", (e) => {
      var
          $btn  = $(e.currentTarget),
          $tr   = $btn.parents(".row-task"),
          $info = $tr.find(".info").eq(0);

      this.onEditTask($info.data("id"));
    });

    // remove
    $(e.currentTarget).on("click", ".btn-remove", (e) => {
      var
          $btn  = $(e.currentTarget),
          $tr   = $btn.parents(".row-task"),
          $info = $tr.find(".info").eq(0);

      this.onRemoveTask($info.data("id"));
    });
  }

  onCreateTableTask(api) {
    this.api_table_task = api;
  }

  onCreateTask() {
    var
        task       = { ...this.state.task },
        modal_task = { ...this.state.modal_task };

    task.id          = '';
    task.date        = new Date();
    task.priority    = 'normal';
    task.content     = '';
    modal_task.show  = true;
    modal_task.title = "Cargar Tarea";

    this.setState({
      modal_task,
      task
    });
  }

  onEditTask(id) {
    this.taskService
      .get(id)
      .then(response => {
        if( !response.data.error ) {
          let
              task  = {...this.state.task},
              parts = response.data.task.date.split("-");

          task.id       = response.data.task.id;
          task.date     = new Date(parseInt(parts[0]), parseInt(parts[1]) - 1, parseInt(parts[2]));
          task.priority = response.data.task.priority;
          task.content  = response.data.task.content;

          this.setState({
            modal_task: {
              show: true,
              title: "Editar Tarea"
            },
            task
          });
        }
        else {
          toast.error(response.data.message);
        }
      })
      .catch(error => {
        toast.error(error);
      });
  }

  onSubmitTask(task, actions) {
    let
        month = task.date.getMonth() + 1,
        date = task.date.getFullYear() + '-' + (month < 10 ? `0${month}` : month) + '-' + (task.date.getDate() < 10 ? `0${task.date.getDate()}` : task.date.getDate());
    
    // insert
    if( task.id === "" ) {
      this.taskService
        .create({
          id_project: this.state.id_project_selected,
          date: date,
          priority: task.priority,
          content: task.content
        })
        .then(response => {
          if( !response.data.error ) {
            let modal_task = { ...this.state.modal_task };

            let projects = this.state.projects.map((p) => {
              if( p.id == this.state.id_project_selected ) {
                p.tasks_count = p.tasks_count + 1;
              }

              return p;
            });

            modal_task.show = false;

            this.reloadTableTasks();

            this.setState({
              projects,
              modal_task,
              task: {
                id: '',
                date: new Date(),
                priority: 'normal',
                content: ''
              }
            });
          }
          else {
            toast.error(response.data.message);
          }
        })
        .catch(error => {
          toast.error(error);
        })
        .finally(() => {
          actions.resetForm();
        });
    }
    // update
    else {
      this.taskService
        .update({
          id: task.id,
          id_project: this.state.id_project_selected,
          date: date,
          priority: task.priority,
          content: task.content
        })
        .then(response => {
          if( !response.data.error ) {
            let modal_task = {...this.state.modal_task};

            modal_task.show = false;

            this.reloadTableTasks();

            this.setState({
              modal_task,
              task: {
                id: '',
                date: new Date(),
                priority: 'normal',
                content: ''
              }
            });
          }
          else {
            toast.error(response.data.message);
          }
        })
        .catch(error => {
          toast.error(error);
        })
        .finally(() => {
          actions.resetForm();
        });
    }
  }

  onCloseModalTask() {
    const modal_task = { ...this.state.modal_task };

    modal_task.show = false;

    this.setState({
      modal_task
    });
  }

  onRemoveTask(id) {
    if( window.confirm("¿Eliminar la Tarea?") ) {
      this.taskService
        .remove(id)
        .then(response => {
          // console.log(response);

          if( !response.data.error ) {
            let projects = this.state.projects.map((p) => {
              if( p.id == this.state.id_project_selected ) {
                p.tasks_count = p.tasks_count - 1;
              }

              return p;
            });
            
            this.setState({
              projects
            });

            this.reloadTableTasks();
          }
          else {
            toast.error(response.data.message);
          }
        })
        .catch(error => {
          toast.error(error);
        });
    }
  }

  reloadTableTasks(type) {
    if( this.api_table_task !== null ) {
      if( type !== undefined ) {
        this.api_table_task.ajax.url(`api/tasks/${type}/datatable`);
      }

      this.api_table_task.ajax.reload();
    }
  }

  onChangeSearchTask(event) {
    let { name, value } = event.target;

    value = value.trim();

    this.setState({
      [name]: value
    });
  }

  render() {
    const options_datatable = {
      processing: true,
      serverSide: true,
      ajax: {
        url: `api/tasks/${this.state.id_project_selected}/datatable`,
        type: "GET",
        data: (data, settings) => {
          data.search.value = this.state.search_tasks;

          return data;
        }
      },
      responsive: true,
      pageLength: 10,
      order: [[0, "desc"]],
      columns: [
        {
          type: "date",
          title: "Fecha",
          width: "110px",
          searchable: false
        },
        {
          type: "string",
          title: "Prioridad",
          width: "110px",
          searchable: false
        },
        {
          type: "string",
          title: "Contenido",
          searchable: true
        },
        {
          type: "string",
          title: "",
          width: "90px",
          orderable: false,
          searchable: false
        }
      ],
      data: [],
      dom: "<'row'<'col-sm-12 col-md-12'>>" +
           "<'row'<'col-sm-12'tr>>" +
           "<'row'<'col-sm-12 col-md-12'p>>",
      language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
          "sFirst":    "Primero",
          "sLast":     "Último",
          "sNext":     "Siguiente",
          "sPrevious": "Anterior"
        },
        "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      },
      createdRow: (row, data, dataIndex, cells) => {
        $(row).addClass("row-task");
      }
    };
    
    return (
      <Fragment>
        <NavBar
          id_project={ this.state.id_project_selected }
          active={ this.state.id_project_selected }
          handleClick={ this.onClickMenu }
          handleClickBtnProject={ this.onCreateProject }
          handleClickBtnTask={ this.onCreateTask }
        />

        <div className="container-fluid">
          <div className="row">
            <div id="projects" className="col-xs-12 col-sm-4 col-md-4 col-lg-2">
              <Projects
                project={ this.state.project }
                data={ this.state.projects }
                id_selected={ this.state.id_project_selected }
                onCreate={ this.onCreateProject }
                onSelect={ this.onSelectProject }
                onEdit={ this.onEditProject }
                onSubmit={ this.onSubmitProject }
                show_modal={ this.state.modal_project.show }
                title_modal={ this.state.modal_project.title }
                onCloseModal={ this.onCloseModalProject }
                onRemove={ this.onRemoveProject }
              />
            </div>
            <div id="tasks" className="col-xs-12 col-sm-8 col-md-8 col-lg-10">
              <div className="row justify-content-md-center">
                <div className="col-4">
                  <form className="form-inline my-2 my-lg-0" onSubmit={ (event) => { event.preventDefault(); this.reloadTableTasks() } }>
                    <input className="form-control form-control-sm mr-sm-2" type="search" name="search_tasks" placeholder="Buscar..." aria-label="Search" onChange={ this.onChangeSearchTask } autoComplete="off" />
                    <button className="btn btn-secondary my-2 my-sm-0 btn-sm" type="submit">Buscar</button>
                  </form>
                </div>
              </div>

              <Tasks
                task={ this.state.task }
                id_project={ this.state.id_project_selected }
                onCreate={ this.onCreateTask }
                onSubmit={ this.onSubmitTask }
                show_modal={ this.state.modal_task.show }
                title_modal={ this.state.modal_task.title }
                onCloseModal={ this.onCloseModalTask }
                options_datatable={ options_datatable }
                onInitTable={ this.onInitTableTasks }
                onCreateTable={ this.onCreateTableTask }
              />
            </div>
          </div>{ /* .row */ }
        </div>{ /* .container-fluid */ }
        <Footer />

        <ToastContainer
          position="top-center"
          autoClose={ 5000 }
        />
      </Fragment>
    );
  }
}

export default App;

if( document.getElementById('app') ) {
  ReactDOM.render(<App />, document.getElementById('app'));
}