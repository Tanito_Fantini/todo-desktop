import 'datatables.net-bs4/css/dataTables.bootstrap4.min.css';
import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

const $ = require('jquery');
$.DataTable = require('datatables.net');
$.DataTableBT = require('datatables.net-bs4');

class Table extends Component {
  constructor(props) {
    super(props);

    // DOM references
    this.table = React.createRef();

    this.api = null;
  }

  // it is invoked immediately after a component is mounted
  componentDidMount() {
    this.api = $(this.table.current).DataTable(this.props.options);

    if( typeof this.props.onCreate === "function" ) {
      this.props.onCreate(this.api);
    }

    if( typeof this.props.onInit === "function" ) {
      this.api.on("init.dt", (e, json) => {
        this.props.onInit(e, json);
      });
    }
  }

  // it is invoked immediately before a component is unmounted and destroyed
  componentWillUnmount(){
    $(this.table.current)
      .DataTable()
      .destroy(true);
  }

  render() {
    return (
      <Fragment>
        <table className="table table-striped table-hover table-sm" ref={this.table} width="100%"></table>
      </Fragment>
    );
  }
}

Table.propTypes = {
  onCreate: PropTypes.func,
  onInit: PropTypes.func
};

export default Table;