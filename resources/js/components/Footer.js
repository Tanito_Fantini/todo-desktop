import React from 'react';
import Logo from '../../images/logo_letras.svg';

const Footer = (props) => (
  <div className="footer text-center">
    <img src={ Logo } alt="logo" />
  </div>
);

export default Footer;