import React from 'react';

const NavBar = (props) => (
  <nav className="navbar fixed-top navbar-expand-lg navbar-dark bg-dark shadow">
    <a className="navbar-brand" href="#">To-Do</a>
    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarText">
      <form className="form-inline my-2 my-lg-0">
        <button className="btn btn-outline-primary my-2 my-sm-0 btn-sm" type="button" title="crear un nuevo Proyecto" onClick={ props.handleClickBtnProject }>
          Proyecto <i className="fas fa-plus"></i>
        </button>
        &nbsp;
        <button className="btn btn-outline-primary my-2 my-sm-0 btn-sm" type="button" title="crear una nueva Tarea" onClick={ props.handleClickBtnTask } disabled={ !(props.id_project > 0) }>
          Tarea <i className="fas fa-plus"></i>
        </button>
      </form>
      <ul className="navbar-nav mr-auto">
        <li className={ 'nav-item' + (props.active == 'hoy' ? ' active' : '') }>
          <a className="nav-link" href="#" onClick={ event => { event.preventDefault(); props.handleClick('hoy'); } } title="listar las tareas de hoy">Hoy</a>
        </li>
        <li className={ 'nav-item' + (props.active == '7dias' ? ' active' : '') }>
          <a className="nav-link" href="#" onClick={ event => { event.preventDefault(); props.handleClick('7dias'); } } title="listar las tareas de los próximos 7 días">Próximos 7 días</a>
        </li>
        <li className={ 'nav-item' + (props.active == 'mes' ? ' active' : '') }>
          <a className="nav-link" href="#" onClick={ event => { event.preventDefault(); props.handleClick('mes'); } } title="listar las tareas de este mes">Este mes</a>
        </li>
      </ul>
      <span className="navbar-text">
        <button type="button" className="btn btn-primary btn-sm" title="Configuracón" onClick={ props.onConfig }>
          <i className="fas fa-tools"></i>
        </button>
      </span>
    </div>
  </nav>
);

export default NavBar;