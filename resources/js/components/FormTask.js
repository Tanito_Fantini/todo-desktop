import React, { Fragment } from 'react';
import DatePicker from 'react-datepicker';
import { Formik } from 'formik';
import * as Yup from 'yup';
import PropTypes from 'prop-types';

import "react-datepicker/dist/react-datepicker.css";

const FormTask = (props) => (
  <Fragment>
    <Formik
      enableReinitialize={ true }
      initialValues={ props.task }
      validationSchema={Yup.object().shape({
        content: Yup.string().trim().max(200, 'La descripción es demasiado larga.').required('Debe ingresar la descripción.')
      })}
      onSubmit={(values, actions) => {
        props.onSubmit(values, actions);
      }}
      render={({
        values,
        errors,
        touched,
        handleChange,
        handleSubmit,
        isSubmitting,
      }) => (
        <form onSubmit={ handleSubmit }>
          <table width="100%">
            <tbody>
              <tr>
                <td width="50%">
                  <div className="form-group">
                    <label htmlFor="date">Fecha</label>
                    <br />
                    <DatePicker
                      selected={typeof values.date === "string" ? new Date(values.date) : values.date}
                      dateFormat="dd/MM/yyyy"
                      className="form-control form-control-sm"
                      placeholderText="Fecha"
                      onChange={(date) => { handleChange({target: {name: "date", value: date}}); }}
                    />
                  </div>
                </td>
                <td>
                  <div className="form-group">
                    <label htmlFor="priority">Prioridad</label>
                    <select type="select" name="priority" id="priority" value={ values.priority } onChange={ handleChange } className="form-control form-control-sm">
                      <option value="low">Baja</option>
                      <option value="normal">Normal</option>
                      <option value="hight">Alta</option>
                    </select>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>

          <div className="form-group">
            <label htmlFor="content">Descripción</label>
            <textarea
              type="textarea"
              name="content"
              id="content"
              value={ values.content }
              ref={ props.input_focus }
              onChange={ handleChange }
              rows="6"
              placeholder="Descripción"
              className="form-control form-control-sm"
            />
            {errors.content && touched.content && <div className="alert alert-danger mt-2" role="alert">{errors.content}</div>}
          </div>

          <div className="text-center">
            <button type="button" className="btn btn-secondary" data-dismiss="modal" disabled={ isSubmitting }>
              Cancelar
            </button>
            &nbsp;
            <button type="submit" className="btn btn-primary" disabled={ isSubmitting }>
              Guardar
            </button>
          </div>
        </form>
      )}
    />
  </Fragment>
);

FormTask.propTypes = {
  task: PropTypes.object,
  onSubmit: PropTypes.func
};

export default FormTask;