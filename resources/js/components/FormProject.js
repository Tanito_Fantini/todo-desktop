import React, { Fragment } from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';
import PropTypes from 'prop-types';

const FormProject = (props) => (
  <Fragment>
    <Formik
      enableReinitialize={ true }
      initialValues={ props.project }
      validationSchema={Yup.object().shape({
        name: Yup.string().min(3, 'El nombre es demasiado corto.').max(50, 'El nombre es demasiado largo.').required('Debe ingresar el nombre.'),
        description: Yup.string().trim().max(255, 'La descripción es demasiado larga.')
      })}
      onSubmit={(values, actions) => {
        props.onSubmit(values, actions);
      }}
      render={({
        values,
        errors,
        touched,
        handleChange,
        handleSubmit,
        isSubmitting,
      }) => (
        <form onSubmit={ handleSubmit }>
          <div className="form-group">
            <label htmlFor="name">Nombre</label>
            <input
              type="text"
              name="name"
              id="name"
              value={ values.name }
              className="form-control form-control-sm"
              ref={ props.input_focus }
              onChange={ handleChange }
              placeholder="Nombre"
              autoComplete="off"
            />
            {errors.name && touched.name && <div className="alert alert-danger mt-2" role="alert">{errors.name}</div>}
          </div>

          <div className="form-group">
            <label htmlFor="description">Descripción</label>
            <textarea
              type="textarea"
              name="description"
              id="description"
              value={ values.description }
              className="form-control form-control-sm"
              onChange={ handleChange }
              rows="6"
              placeholder="Descripción"
            />
            {errors.description && touched.description && <div className="alert alert-danger mt-2" role="alert">{errors.description}</div>}
          </div>

          <div className="text-center">
            <button type="button" className="btn btn-secondary" data-dismiss="modal" disabled={ isSubmitting }>
              Cancelar
            </button>
            &nbsp;
            <button type="submit" className="btn btn-primary" disabled={ isSubmitting }>
              Guardar
            </button>
          </div>
        </form>
      )}
    />
  </Fragment>
);

FormProject.propTypes = {
  project: PropTypes.object,
  onSubmit: PropTypes.func
};

export default FormProject;