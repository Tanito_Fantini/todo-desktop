import axios from 'axios'

class Task {
  constructor() {
  }

  get(id) {
    return axios({
      method: 'get',
      url: '/api/tasks/' + id
    });
  }

  create(task) {
    return axios({
      method: 'post',
      url: '/api/tasks',
      data: task
    });
  }

  update(task) {
    return axios({
      method: 'put',
      url: '/api/tasks/' + task.id,
      data: task
    });
  }

  remove(id) {
    return axios({
      method: 'delete',
      url: '/api/tasks/' + id
    });
  }
}

export default Task;