<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('tasks', function(Blueprint $table) {
      $table->bigIncrements('id');
      $table->unsignedBigInteger('project_id');
      $table->date('date');
      $table->enum('priority', ['low', 'normal', 'hight']);
      $table->text('content');
      $table->timestamps();

      $table->foreign('project_id')
            ->references('id')
            ->on('projects')
            ->onUpdate('cascade')
            ->onDelete('restrict');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('tasks');
  }
}
