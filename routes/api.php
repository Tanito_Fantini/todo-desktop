<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('projects', 'ProjectController@index');
Route::post('projects', 'ProjectController@store');
Route::get('projects/{id}', 'ProjectController@show');
Route::put('projects/{id}', 'ProjectController@store');
Route::delete('projects/{id}', 'ProjectController@remove');

Route::post('tasks', 'TaskController@store');
Route::get('tasks/{id}', 'TaskController@show');
Route::get('tasks/{id}/datatable', 'TaskController@datatable');
Route::put('tasks/{id}', 'TaskController@store');
Route::delete('tasks/{id}', 'TaskController@remove');
